﻿#include "app.h"
#include "gui/main.h"

using namespace VisioRealm;

wxIMPLEMENT_APP(ProjectApp);

#include <wx/display.h>

bool ProjectApp::OnInit()
{
	wxRect tempObject = wxDisplay((unsigned int) 0).GetClientArea();
	tempObject.width = tempObject.width * 0.85;
	tempObject.height = tempObject.height * 0.85;

	MainGUI *FrameMain = new MainGUI(0, wxID_ANY, wxT("VisioRealm"), wxDefaultPosition, wxSize(tempObject.width, tempObject.height));
	FrameMain->Show(true);
	return true;
}