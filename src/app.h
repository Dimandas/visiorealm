﻿#pragma once

#include <wx/app.h>

namespace VisioRealm
{
	class ProjectApp : public wxApp
	{
	public:
		virtual bool OnInit();
	};
}