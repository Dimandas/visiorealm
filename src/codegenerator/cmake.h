#pragma once

#include <string>
#include <fstream>

namespace VisioRealm
{
	class CMake
	{
	public:
		CMake(std::string& path)
		{
			m_path = path;
		}

		~CMake();

		void CreateCMakeListsFile();
		void GenerateCMakeCode();

	protected:
		std::string m_path;

		std::ofstream m_outFile;
		bool m_fileCreate = false;
		
		void  CreateFindOpenCVFile();
	};
}