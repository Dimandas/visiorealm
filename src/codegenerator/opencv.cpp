#include "opencv.h"

#include <filesystem>

using namespace VisioRealm;

// TODO �������� ��������� ������ �� ������ ������������� �������� �����/�����
OpenCV::~OpenCV()
{
	if (m_fileHCreate == true)
	{
		m_outHFile.close();
	}

	if (m_fileCPPCreate == true)
	{
		m_outCPPFile.close();
	}
}

void OpenCV::CreateHFile()
{
	if (m_fileHCreate == true)
	{
		return;
	}

	std::experimental::filesystem::create_directory("include"); // TODO � ������� filesystem ����� ��������� �� experimental
	m_outHFile.open(m_path + "/include/libopencv.h", std::ios_base::trunc);
	m_fileHCreate = true;
}

void OpenCV::CreateCPPFile()
{
	if (m_fileCPPCreate == true)
	{
		return;
	}

	std::experimental::filesystem::create_directory("src"); // TODO � ������� filesystem ����� ��������� �� experimental
	m_outCPPFile.open(m_path + "/src/libopencv.cpp", std::ios_base::trunc);
	m_fileCPPCreate = true;
}

void OpenCV::GenerateInitCode()
{
	if (m_fileHCreate == false)
	{
		return;
	}

	if (m_fileCPPCreate == false)
	{
		return;
	}

	m_outHFile << R"()"; // TODO

	m_outCPPFile << R"()"; // TODO
}