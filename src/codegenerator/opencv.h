#pragma once

#include <string>
#include <fstream>

namespace VisioRealm
{
	class OpenCV
	{
	public:
		OpenCV(std::string& path)
		{
			m_path = path;
		}

		~OpenCV();

		void CreateHFile();
		void CreateCPPFile();
		void GenerateInitCode();

	protected:
		std::string m_path;

		std::ofstream m_outCPPFile;
		bool m_fileCPPCreate = false;

		std::ofstream m_outHFile;
		bool m_fileHCreate = false;
	};
}