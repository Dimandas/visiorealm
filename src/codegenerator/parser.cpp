#include "parser.h"

#include "cmake.h"
#include "opencv.h"

using namespace VisioRealm;

void Parser::Generate()
{
	CMake genCmake(m_path);
	genCmake.CreateCMakeListsFile();
	genCmake.GenerateCMakeCode();

	OpenCV genOpenCV(m_path);
	genOpenCV.CreateHFile();
	genOpenCV.CreateCPPFile();
	// TODO
}