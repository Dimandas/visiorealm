#pragma once

#include "../gui/main.h"

namespace VisioRealm
{
	class Parser
	{
	public:
		Parser(wxSFDiagramManager* manager, std::string& path)
		{
			m_manager = manager;
			m_path = path;
		}

		void Generate();

	protected:
		wxSFDiagramManager* m_manager;
		std::string m_path;
	};
}