﻿#include "changebrightness.h"

#include <opencv2/imgcodecs.hpp>
#include "../utils/convertimage.h"

using namespace VisioRealm;

ChangeBrightness::ChangeBrightness(int brightnessLevel, wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	m_previewImage = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize(550, 510), 0);
	bSizer2->Add(m_previewImage, 0, wxALL, 5);


	bSizer1->Add(bSizer2, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer(wxVERTICAL);


	bSizer7->Add(0, 0, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer7, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer(wxVERTICAL);

	m_textBrightness = new wxStaticText(this, wxID_ANY, wxT("Яркость:"), wxDefaultPosition, wxSize(-1, -1), 0);
	m_textBrightness->Wrap(-1);
	m_textBrightness->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString));

	bSizer6->Add(m_textBrightness, 0, wxALIGN_LEFT | wxALL, 5);


	bSizer5->Add(bSizer6, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer(wxVERTICAL);

	m_showLevel = new wxTextCtrl(this, wxID_ANY, wxT("0"), wxDefaultPosition, wxSize(70, 28), wxTE_PROCESS_ENTER, wxTextValidator(wxFILTER_NUMERIC, nullptr));

#ifdef __WXGTK__
	if (!m_showLevel->HasFlag(wxTE_MULTILINE))
	{
		m_showLevel->SetMaxLength(4);
	}
#else
	m_showLevel->SetMaxLength(4);
#endif
	bSizer17->Add(m_showLevel, 0, wxALIGN_RIGHT | wxALL, 5);


	bSizer5->Add(bSizer17, 1, wxEXPAND, 5);


	bSizer3->Add(bSizer5, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer(wxVERTICAL);

	m_level = new wxSlider(this, wxID_ANY, 0, -150, 150, wxDefaultPosition, wxSize(-1, 27), wxSL_HORIZONTAL);
	bSizer18->Add(m_level, 0, wxALL | wxEXPAND, 5);


	bSizer3->Add(bSizer18, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer3, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer(wxVERTICAL);


	bSizer12->Add(0, 0, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer12, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer30;
	bSizer30 = new wxBoxSizer(wxHORIZONTAL);


	bSizer30->Add(0, 0, 1, wxEXPAND, 5);

	m_apply = new wxButton(this, wxID_ANY, wxT("Применить"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer30->Add(m_apply, 0, wxALIGN_BOTTOM | wxALL, 5);

	m_cancel = new wxButton(this, wxID_ANY, wxT("Отменить"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer30->Add(m_cancel, 0, wxALIGN_BOTTOM | wxALL, 5);


	bSizer4->Add(bSizer30, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer4, 1, wxEXPAND, 5);


	this->SetSizer(bSizer1);
	this->Layout();

	this->Centre(wxBOTH);

	// Connect Events
	m_showLevel->Connect(wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler(ChangeBrightness::m_showLevelOnTextEnter), NULL, this);
	m_level->Connect(wxEVT_SCROLL_TOP, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Connect(wxEVT_SCROLL_BOTTOM, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Connect(wxEVT_SCROLL_LINEUP, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Connect(wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Connect(wxEVT_SCROLL_PAGEUP, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Connect(wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Connect(wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Connect(wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Connect(wxEVT_SCROLL_CHANGED, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_apply->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ChangeBrightness::m_applyOnButtonClick), NULL, this);
	m_cancel->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ChangeBrightness::m_cancelOnButtonClick), NULL, this);

	m_originalImage = cv::imread("test.png");

	m_brightnessLevel = brightnessLevel;
	m_level->SetValue(m_brightnessLevel);
	m_showLevel->SetLabelText(std::to_string(m_brightnessLevel));

	ChangeBrightnessImage(m_brightnessLevel);
	DisplayPreviewImage();
}

ChangeBrightness::~ChangeBrightness()
{
	// Disconnect Events
	m_showLevel->Disconnect(wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler(ChangeBrightness::m_showLevelOnTextEnter), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_TOP, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_BOTTOM, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_LINEUP, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_PAGEUP, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_level->Disconnect(wxEVT_SCROLL_CHANGED, wxScrollEventHandler(ChangeBrightness::m_levelOnScroll), NULL, this);
	m_apply->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ChangeBrightness::m_applyOnButtonClick), NULL, this);
	m_cancel->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ChangeBrightness::m_cancelOnButtonClick), NULL, this);

}

void ChangeBrightness::m_showLevelOnTextEnter(wxCommandEvent& event)
{
	if (wxAtoi(event.GetString()) == m_brightnessLevel && event.GetString() == std::to_string(wxAtoi(event.GetString()))) return;

	m_level->SetValue(wxAtoi(event.GetString()));
	m_showLevel->SetLabelText(std::to_string(wxAtoi(event.GetString())));

	ChangeBrightnessImage(wxAtoi(event.GetString()));
	DisplayPreviewImage();

	event.Skip();
}

void ChangeBrightness::m_levelOnScroll(wxScrollEvent& event)
{
	if (event.GetPosition() == m_brightnessLevel) return;

	m_showLevel->SetLabelText(std::to_string(event.GetPosition()));

	ChangeBrightnessImage(event.GetPosition());
	DisplayPreviewImage();

	event.Skip();
}

void ChangeBrightness::m_applyOnButtonClick(wxCommandEvent& event)
{
	EndModal(wxID_APPLY);

	event.Skip();
}

void ChangeBrightness::m_cancelOnButtonClick(wxCommandEvent& event)
{
	EndModal(wxID_CANCEL);

	event.Skip();
}

void ChangeBrightness::DisplayPreviewImage()
{
	this->Update();

	m_outputImage = ConvertBGRAToRGB(m_changedImage);

	m_previewImage->SetBitmap((wxBitmap)m_outputImage);

	m_previewImage->SetSize(wxSize(550, 510));    // костыль

	this->Update();
}

void ChangeBrightness::ChangeBrightnessImage(double newLevel)
{
	m_brightnessLevel = newLevel;

	/*if (newLevel == 150)
	{
		newLevel = 254;
	}
	else if (newLevel == -150)
	{
		newLevel = -1;
	} */
	if (newLevel >= 0)
	{
		newLevel = newLevel * positiveStepBrightness;
	}
	else if (newLevel < 0)
	{
		newLevel = newLevel * negativeStepBrightness;
	}

	m_changedImage = m_originalImage * (1 + newLevel);
}

XS_IMPLEMENT_CLONABLE_CLASS(FlowchartChangeBrightness, RoundedSquareShape);

FlowchartChangeBrightness::FlowchartChangeBrightness(int brightnessLevel)
{
	AcceptConnection(wxT("All"));
	AcceptSrcNeighbour(wxT("All"));
	AcceptTrgNeighbour(wxT("All"));

	CaptionText = new wxSFTextShape();

	CaptionText->SetVAlign(wxSFShapeBase::valignMIDDLE);
	CaptionText->SetHAlign(wxSFShapeBase::halignCENTER);

	CaptionText->SetStyle(sfsALWAYS_INSIDE | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION | sfsPROPAGATE_INTERACTIVE_CONNECTION);

	CaptionText->AddStyle(wxSFShapeBase::sfsEMIT_EVENTS);

	m_brightnessLevel = brightnessLevel;

	CaptionText->SetText(wxT("   Изменить яркость (" + std::to_string(m_brightnessLevel) + ")" + "   "));

	SF_ADD_COMPONENT(CaptionText, wxT("caption"));

	MarkSerializableDataMembers();
}

void FlowchartChangeBrightness::MarkSerializableDataMembers()
{
	XS_SERIALIZE_EX(m_brightnessLevel, wxT("brightness"), 15);   // вместо 15 выбрать другой код с помощью константы
}