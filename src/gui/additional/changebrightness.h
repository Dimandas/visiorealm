﻿#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/slider.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <opencv2/core.hpp>

#include "../flowchart/shapes/roundedsquare.h"

namespace VisioRealm
{
	class ChangeBrightness : public wxDialog
	{
	private:

	protected:
		wxStaticBitmap* m_previewImage;
		wxStaticText* m_textBrightness;
		wxTextCtrl* m_showLevel;
		wxSlider* m_level;
		wxButton* m_apply;
		wxButton* m_cancel;

		int m_brightnessLevel = 0;
		cv::Mat m_originalImage;
		cv::Mat m_changedImage;
		wxImage m_outputImage;

		void DisplayPreviewImage();
		virtual void m_showLevelOnTextEnter(wxCommandEvent& event);
		virtual void m_levelOnScroll(wxScrollEvent& event);
		virtual void m_applyOnButtonClick(wxCommandEvent& event);
		virtual void m_cancelOnButtonClick(wxCommandEvent& event);

		void inline ChangeBrightnessImage(double newLevel);

		static constexpr double positiveStepBrightness = 0.008;
		static constexpr double negativeStepBrightness = 0.006;

	public:

		ChangeBrightness(int brightnessLevel, wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Изменить яркость"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(550, 721), long style = wxDEFAULT_DIALOG_STYLE);
		~ChangeBrightness();

		int inline GetChangeBrightness()
		{
			return m_brightnessLevel;
		}

	};

	class  FlowchartChangeBrightness : public RoundedSquareShape
	{
	public:
		XS_DECLARE_CLONABLE_CLASS(FlowchartChangeBrightness);

		FlowchartChangeBrightness() :FlowchartChangeBrightness(0) {};
		FlowchartChangeBrightness(int brightnessLevel);
		~FlowchartChangeBrightness() { ; }
		int test() { ; }

	private:
		int m_brightnessLevel;

		void MarkSerializableDataMembers();
	};
}