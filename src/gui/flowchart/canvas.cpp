﻿#include "canvas.h"

// Подключение диалогов
#include "../additional/changebrightness.h"

using namespace VisioRealm;

FlowchartCanvas::FlowchartCanvas(wxSFDiagramManager* manager, wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
	:wxSFShapeCanvas(manager, parent, id, pos, size, style)
{
	m_startingCoordinates = { 80, 0 };

	manager->ClearAcceptedShapes();
	manager->AcceptShape(wxT("All"));

	AddStyle(sfsGRID_SHOW);
	AddStyle(sfsGRID_USE);
	AddStyle(sfsPROCESS_MOUSEWHEEL);
	SetMinScale(ScaleMinCanvas);
	SetMaxScale(ScaleMaxCanvas);
}

bool FlowchartCanvas::CreateShapeObject(const wxString& nameObject, wxSFDiagramManager* manager)
{
	// раздел действий
	if (nameObject == "Изменить яркость")
	{
		ChangeBrightness* dialogBrightness = new ChangeBrightness(0, this);
		if (dialogBrightness->ShowModal() == wxID_APPLY)
		{
			int tempObject = dialogBrightness->GetChangeBrightness();

			manager->AddShape(new FlowchartChangeBrightness(tempObject), NULL, GiveCoordinates(), sfINITIALIZE);

			dialogBrightness->Destroy();
			return true;
		}
		else
		{
			dialogBrightness->Destroy();
			return false;
		}
	}
	// ----------

	//раздел условий
	if (nameObject == "Число A равно числу B")
	{

	}
	// ----------

	//раздел событий
	if (nameObject == "Срабатывание таймера")
	{

	}
	// -----------

	return false;
}

wxPoint FlowchartCanvas::GiveCoordinates()
{
	wxSize tempObject = GetClientSize();
	if (m_startingCoordinates.x + 60 < tempObject.x && m_startingCoordinates.y + 60 < tempObject.y)
	{
		m_startingCoordinates.x = m_startingCoordinates.x + 20;
		m_startingCoordinates.y = m_startingCoordinates.y + 20;
	}
	else
	{
		m_startingCoordinates.x = 80;
		m_startingCoordinates.y = 0;
	}

	return m_startingCoordinates;
}

void FlowchartCanvas::EditShapeObject(wxSFShapeMouseEvent& event)
{
	wxSFShapeBase* eventShape = event.GetShape();
	wxSFShapeBase* mainShape = eventShape->GetGrandParentShape();

	if (mainShape->GetClassInfo() == CLASSINFO(FlowchartChangeBrightness))
	{
		int oldBrightness = mainShape->GetProperty("brightness")->AsInt();
		ChangeBrightness* dialogBrightness = new ChangeBrightness(oldBrightness, this);

		if (dialogBrightness->ShowModal() == wxID_APPLY)
		{
			int tempObject = dialogBrightness->GetChangeBrightness();

			int& newBrightness = mainShape->GetProperty("brightness")->AsInt();
			newBrightness = tempObject;

			wxString& newText = mainShape->GetFirstChild(CLASSINFO(wxSFTextShape))->GetProperty("text")->AsString();
			newText = "   Изменить яркость (" + std::to_string(tempObject) + ")" + "   ";

			wxSFShapeBase* textShape = (wxSFShapeBase*)mainShape->GetFirstChild(CLASSINFO(wxSFTextShape));
			textShape->Update();
			mainShape->Refresh();

			SaveCanvasState();

			dialogBrightness->Destroy();
			return;
		}
		else
		{
			dialogBrightness->Destroy();
			return;
		}
	}

	event.Skip();
}

void FlowchartCanvas::OnConnectionFinished(wxSFLineShape* connection)
{
	if (connection == nullptr)
		return;

	connection->SetSrcArrow(CLASSINFO(wxSFCircleArrow));
	connection->SetTrgArrow(CLASSINFO(wxSFSolidArrow));
}