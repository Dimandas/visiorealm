﻿#pragma once

#include <wx/wxsf/wxShapeFramework.h>

namespace VisioRealm
{
	class FlowchartCanvas : public wxSFShapeCanvas
	{

	public:
		FlowchartCanvas(wxSFDiagramManager* manager, wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL);
		~FlowchartCanvas() { ; }
		inline const double GetDefaultScale() { return ScaleDefaultCanvas; }
		inline const double GetStepScale() { return ScaleStepCanvas; }
		bool CreateShapeObject(const wxString& nameObject, wxSFDiagramManager* manager);
		void EditShapeObject(wxSFShapeMouseEvent& event);

	protected:
		wxPoint GiveCoordinates();
		wxPoint m_startingCoordinates;

		void OnConnectionFinished(wxSFLineShape* connection);


	private:
		static constexpr double ScaleStepCanvas = 0.2;
		static constexpr double ScaleDefaultCanvas = 1;
		static constexpr double ScaleMaxCanvas = 6;
		static constexpr double ScaleMinCanvas = 0.6;

	};
}