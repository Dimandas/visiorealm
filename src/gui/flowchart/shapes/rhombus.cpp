﻿#include "rhombus.h"

using namespace VisioRealm;

XS_IMPLEMENT_CLONABLE_CLASS(RhombusShape, wxSFPolygonShape);

const wxRealPoint RhombusShape::RhombusProjection[] = { wxRealPoint(0,0), wxRealPoint(50,-25), wxRealPoint(100, 0), wxRealPoint(50, 25) };

RhombusShape::RhombusShape()
{
	EnablePropertySerialization(wxT("vertices"), false);

	SetVertices(4, RhombusProjection);

	AddConnectionPoint(wxSFConnectionPoint::cpTOPMIDDLE);
	AddConnectionPoint(wxSFConnectionPoint::cpCENTERLEFT);
	AddConnectionPoint(wxSFConnectionPoint::cpCENTERRIGHT);

	AcceptConnection(wxT("All"));
	AcceptSrcNeighbour(wxT("All"));
	AcceptTrgNeighbour(wxT("All"));

	CaptionText = new wxSFEditTextShape();
	CaptionText->SetText(wxT("Условие"));

	CaptionText->SetVAlign(wxSFShapeBase::valignMIDDLE);
	CaptionText->SetHAlign(wxSFShapeBase::halignCENTER);
	CaptionText->Scale(0.82, 0.82);     // костыль

	CaptionText->SetStyle(sfsALWAYS_INSIDE | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION | sfsPROPAGATE_INTERACTIVE_CONNECTION);
	CaptionText->SetEditType(wxSFEditTextShape::editDISABLED);

	SF_ADD_COMPONENT(CaptionText, wxT("caption"));
}