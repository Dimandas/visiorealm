﻿#pragma once

#include <wx/wxsf/wxShapeFramework.h>

#include <wx/gdicmn.h>     // wxRealPoint


namespace VisioRealm
{
	class RhombusShape : public wxSFPolygonShape
	{
	public:
		XS_DECLARE_CLONABLE_CLASS(RhombusShape);

		RhombusShape();
		~RhombusShape() { ; }


	protected:
		wxSFEditTextShape* CaptionText;

	private:
		static const wxRealPoint RhombusProjection[];

	};
}