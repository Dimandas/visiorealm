﻿#include "roundedsquare.h"


using namespace VisioRealm;

XS_IMPLEMENT_CLONABLE_CLASS(RoundedSquareShape, wxSFRoundRectShape);

RoundedSquareShape::RoundedSquareShape()
{
	EnablePropertySerialization(wxT("vertices"), false);
	AddConnectionPoint(wxSFConnectionPoint::cpTOPMIDDLE);
	AddConnectionPoint(wxSFConnectionPoint::cpBOTTOMMIDDLE);
	AddStyle(wxSFShapeBase::sfsEMIT_EVENTS);
}