﻿#pragma once

#include <wx/wxsf/wxShapeFramework.h>


namespace VisioRealm
{
	class RoundedSquareShape : public wxSFRoundRectShape
	{
	public:
		XS_DECLARE_CLONABLE_CLASS(RoundedSquareShape);

		RoundedSquareShape();
		~RoundedSquareShape() { ; }


	protected:
		wxSFTextShape* CaptionText;

	};
}