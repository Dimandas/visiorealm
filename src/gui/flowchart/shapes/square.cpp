﻿#include "square.h"


using namespace VisioRealm;

XS_IMPLEMENT_CLONABLE_CLASS(SquareShape, wxSFRectShape);

SquareShape::SquareShape()
{
	EnablePropertySerialization(wxT("vertices"), false);

	AcceptConnection(wxT("All"));
	AcceptSrcNeighbour(wxT("All"));
	AcceptTrgNeighbour(wxT("All"));

	CaptionText = new wxSFEditTextShape();
	CaptionText->SetText(wxT("Действие"));

	CaptionText->SetVAlign(wxSFShapeBase::valignMIDDLE);
	CaptionText->SetHAlign(wxSFShapeBase::halignCENTER);
	CaptionText->Scale(0.82, 0.82);     // костыль

	CaptionText->SetStyle(sfsALWAYS_INSIDE | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION | sfsPROPAGATE_INTERACTIVE_CONNECTION);
	CaptionText->SetEditType(wxSFEditTextShape::editDISABLED);

	SF_ADD_COMPONENT(CaptionText, wxT("caption"));
}