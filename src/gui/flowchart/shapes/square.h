﻿#pragma once

#include <wx/wxsf/wxShapeFramework.h>


namespace VisioRealm
{
	class SquareShape : public wxSFRectShape
	{
	public:
		XS_DECLARE_CLONABLE_CLASS(SquareShape);

		SquareShape();
		~SquareShape() { ; }


	protected:
		wxSFEditTextShape* CaptionText;

	};
}