﻿#include <wx/dirdlg.h>

#include "main.h"

#include "../codegenerator/parser.h"

using namespace VisioRealm;

MainGUI::MainGUI(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);
	m_mgr.SetManagedWindow(this);
	m_mgr.SetFlags(wxAUI_MGR_DEFAULT);

	SearchMain = new wxSearchCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(248, 32), 0);
#ifndef __WXMAC__
	SearchMain->ShowSearchButton(false);
#endif
	SearchMain->ShowCancelButton(true);
	m_mgr.AddPane(SearchMain, wxAuiPaneInfo().Left().CaptionVisible(false).CloseButton(false).PaneBorder(false).Movable(false).Dock().Fixed().DockFixed(true).BottomDockable(false).TopDockable(false).LeftDockable(false).RightDockable(false).Floatable(false));

	ToolBoxMain = new wxTreeCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE | wxTR_HIDE_ROOT);
	m_mgr.AddPane(ToolBoxMain, wxAuiPaneInfo().Left().CaptionVisible(false).CloseButton(false).PaneBorder(false).Movable(false).Dock().Resizable().FloatingSize(wxDefaultSize).DockFixed(true).BottomDockable(false).TopDockable(false).LeftDockable(false).RightDockable(false).Floatable(false));

	MenubarMain = new wxMenuBar(0);
	FileMenu1Main = new wxMenu();
	wxMenuItem* NewProjectMenuItemMain;
	NewProjectMenuItemMain = new wxMenuItem(FileMenu1Main, wxID_ANY, wxString(wxT("Новый проект")), wxEmptyString, wxITEM_NORMAL);
	FileMenu1Main->Append(NewProjectMenuItemMain);

	wxMenuItem* OpenMenuItemMain;
	OpenMenuItemMain = new wxMenuItem(FileMenu1Main, wxID_ANY, wxString(wxT("Открыть")), wxEmptyString, wxITEM_NORMAL);
	FileMenu1Main->Append(OpenMenuItemMain);

	FileMenu1Main->AppendSeparator();

	wxMenuItem* SaveMenuItemMain;
	SaveMenuItemMain = new wxMenuItem(FileMenu1Main, wxID_ANY, wxString(wxT("Сохранить")), wxEmptyString, wxITEM_NORMAL);
	FileMenu1Main->Append(SaveMenuItemMain);

	wxMenuItem* SaveAsMenuItemMain;
	SaveAsMenuItemMain = new wxMenuItem(FileMenu1Main, wxID_ANY, wxString(wxT("Сохранить как")), wxEmptyString, wxITEM_NORMAL);
	FileMenu1Main->Append(SaveAsMenuItemMain);

	FileMenu1Main->AppendSeparator();

	wxMenuItem* m_generateCode;
	m_generateCode = new wxMenuItem(FileMenu1Main, wxID_ANY, wxString(wxT("Сгенерировать код")), wxEmptyString, wxITEM_NORMAL);
	FileMenu1Main->Append(m_generateCode);

	FileMenu1Main->AppendSeparator();

	wxMenuItem* ExitMenuItemMain;
	ExitMenuItemMain = new wxMenuItem(FileMenu1Main, wxID_ANY, wxString(wxT("Выход")), wxEmptyString, wxITEM_NORMAL);
	FileMenu1Main->Append(ExitMenuItemMain);

	MenubarMain->Append(FileMenu1Main, wxT("Файл"));

	EditMenu2Main = new wxMenu();
	//
	CancelMenuItemMain = new wxMenuItem(EditMenu2Main, wxID_ANY, wxString(wxT("Отменить")), wxEmptyString, wxITEM_NORMAL);
	EditMenu2Main->Append(CancelMenuItemMain);

	//
	BackMenuItemMain = new wxMenuItem(EditMenu2Main, wxID_ANY, wxString(wxT("Вернуть")), wxEmptyString, wxITEM_NORMAL);
	EditMenu2Main->Append(BackMenuItemMain);

	EditMenu2Main->AppendSeparator();

	wxMenuItem* DeleteAllMenuItemMain;
	DeleteAllMenuItemMain = new wxMenuItem(EditMenu2Main, wxID_ANY, wxString(wxT("Удалить все")), wxEmptyString, wxITEM_NORMAL);
	EditMenu2Main->Append(DeleteAllMenuItemMain);

	MenubarMain->Append(EditMenu2Main, wxT("Правка"));

	ViewMenu3Main = new wxMenu();
	wxMenuItem* ZoomInMenuItemMain;
	ZoomInMenuItemMain = new wxMenuItem(ViewMenu3Main, wxID_ANY, wxString(wxT("Увеличить масштаб")), wxEmptyString, wxITEM_NORMAL);
	ViewMenu3Main->Append(ZoomInMenuItemMain);

	wxMenuItem* ZoomOutMenuItemMain;
	ZoomOutMenuItemMain = new wxMenuItem(ViewMenu3Main, wxID_ANY, wxString(wxT("Уменьшить масштаб")), wxEmptyString, wxITEM_NORMAL);
	ViewMenu3Main->Append(ZoomOutMenuItemMain);

	wxMenuItem* DefaultScaleMenuItemMain;
	DefaultScaleMenuItemMain = new wxMenuItem(ViewMenu3Main, wxID_ANY, wxString(wxT("Масштаб по умолчанию")), wxEmptyString, wxITEM_NORMAL);
	ViewMenu3Main->Append(DefaultScaleMenuItemMain);

	MenubarMain->Append(ViewMenu3Main, wxT("Вид"));

	InformationMenu4Main = new wxMenu();
	wxMenuItem* AboutMenuItemMain;
	AboutMenuItemMain = new wxMenuItem(InformationMenu4Main, wxID_ANY, wxString(wxT("О программе")), wxEmptyString, wxITEM_NORMAL);
	InformationMenu4Main->Append(AboutMenuItemMain);

	MenubarMain->Append(InformationMenu4Main, wxT("Справка"));

	this->SetMenuBar(MenubarMain);

	// Added user code
	m_pathSaveData = wxEmptyString;

	SearchMain->SetDescriptiveText(wxT("Поиск"));

	wxTreeItemId ToolBoxMain_ItemRoot = ToolBoxMain->AddRoot(wxT("root"));

	ToolBoxMain_List.Create(ToolBoxMain, ToolBoxMain_ItemRoot);
	ToolBoxMain_List.Show();

	FlowchartCanvasMain = new FlowchartCanvas(&FlowchartManager, this, this->GetId());
	m_mgr.AddPane(FlowchartCanvasMain, wxAuiPaneInfo().Center().CaptionVisible(false).CloseButton(false).PaneBorder(false).Movable(false).Dock().Resizable().FloatingSize(wxDefaultSize).DockFixed(true).BottomDockable(false).TopDockable(false).LeftDockable(false).RightDockable(false).Floatable(false));
	//////////

	m_mgr.Update();
	this->Centre(wxBOTH);

	// Connect Events
	FlowchartCanvasMain->Connect(wxEVT_SF_SHAPE_LEFT_DCLICK, wxSFShapeMouseEventHandler(MainGUI::OnShapeLeftDoubleClick), NULL, this);
	FlowchartCanvasMain->Connect(wxEVT_SF_SHAPE_RIGHT_DOWN, wxSFShapeMouseEventHandler(MainGUI::OnShapeRightClick), NULL, this);
	FlowchartCanvasMain->Connect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(MainGUI::OnCanvasRightClick), NULL, this);
	//FlowchartCanvasMain->Connect(wxEVT_SF_LINE_DONE, wxSFShapeChildDropEventHandler(MainGUI::OnConnectionFinished), NULL, this);
	//
	this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(MainGUI::MainGUIOnClose));
	SearchMain->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(MainGUI::SearchMainOnText), NULL, this);
	ToolBoxMain->Connect(wxEVT_COMMAND_TREE_ITEM_ACTIVATED, wxTreeEventHandler(MainGUI::ToolBoxMainOnTreeItemActivated), NULL, this);
	FileMenu1Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::NewProjectMenuItemMainOnMenuSelection), this, NewProjectMenuItemMain->GetId());
	FileMenu1Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::OpenMenuItemMainOnMenuSelection), this, OpenMenuItemMain->GetId());
	FileMenu1Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::SaveMenuItemMainOnMenuSelection), this, SaveMenuItemMain->GetId());
	FileMenu1Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::SaveAsMenuItemMainOnMenuSelection), this, SaveAsMenuItemMain->GetId());
	FileMenu1Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::m_generateCodeOnMenuSelection), this, m_generateCode->GetId());
	FileMenu1Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::ExitMenuItemMainOnMenuSelection), this, ExitMenuItemMain->GetId());
	EditMenu2Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::CancelMenuItemMainOnMenuSelection), this, CancelMenuItemMain->GetId());
	this->Connect(CancelMenuItemMain->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(MainGUI::CancelMenuItemMainOnUpdateUI));
	EditMenu2Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::BackMenuItemMainOnMenuSelection), this, BackMenuItemMain->GetId());
	this->Connect(BackMenuItemMain->GetId(), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(MainGUI::BackMenuItemMainOnUpdateUI));
	EditMenu2Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::DeleteAllMenuItemMainOnMenuSelection), this, DeleteAllMenuItemMain->GetId());
	ViewMenu3Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::ZoomInMenuItemMainOnMenuSelection), this, ZoomInMenuItemMain->GetId());
	ViewMenu3Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::ZoomOutMenuItemMainOnMenuSelection), this, ZoomOutMenuItemMain->GetId());
	ViewMenu3Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::DefaultScaleMenuItemMainOnMenuSelection), this, DefaultScaleMenuItemMain->GetId());
	InformationMenu4Main->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MainGUI::AboutMenuItemMainOnMenuSelection), this, AboutMenuItemMain->GetId());
}

MainGUI::~MainGUI()
{
	// Disconnect Events
	this->Disconnect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(MainGUI::MainGUIOnClose));
	SearchMain->Disconnect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(MainGUI::SearchMainOnText), NULL, this);
	ToolBoxMain->Disconnect(wxEVT_COMMAND_TREE_ITEM_ACTIVATED, wxTreeEventHandler(MainGUI::ToolBoxMainOnTreeItemActivated), NULL, this);
	this->Disconnect(wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler(MainGUI::CancelMenuItemMainOnUpdateUI));
	this->Disconnect(wxID_ANY, wxEVT_UPDATE_UI, wxUpdateUIEventHandler(MainGUI::BackMenuItemMainOnUpdateUI));

	m_mgr.UnInit();
}

void MainGUI::ZoomInMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	if (FlowchartCanvasMain->GetScale() + FlowchartCanvasMain->GetStepScale() < FlowchartCanvasMain->GetMaxScale())
	{
		FlowchartCanvasMain->SetScale(FlowchartCanvasMain->GetScale() + FlowchartCanvasMain->GetStepScale());
		FlowchartCanvasMain->Refresh();
	}
	else
	{
		wxMessageBox(wxT("Максимальный масштаб."), this->GetTitle(), wxOK | wxCENTRE | wxICON_INFORMATION);
	}

	event.Skip();
}

void MainGUI::ZoomOutMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	if (FlowchartCanvasMain->GetScale() - FlowchartCanvasMain->GetStepScale() > FlowchartCanvasMain->GetMinScale())
	{
		FlowchartCanvasMain->SetScale(FlowchartCanvasMain->GetScale() - FlowchartCanvasMain->GetStepScale());
		FlowchartCanvasMain->Refresh();
	}
	else
	{
		wxMessageBox(wxT("Минимальный масштаб."), this->GetTitle(), wxOK | wxCENTRE | wxICON_INFORMATION);
	}

	event.Skip();
}

void MainGUI::DefaultScaleMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	FlowchartCanvasMain->SetScale(FlowchartCanvasMain->GetDefaultScale());
	FlowchartCanvasMain->Refresh();

	event.Skip();
}

void MainGUI::ExitMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	this->Close();

	event.Skip();
}

void MainGUI::CancelMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	FlowchartCanvasMain->Undo();
	FlowchartCanvasMain->Refresh();

	event.Skip();
}

void MainGUI::BackMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	FlowchartCanvasMain->Redo();
	FlowchartCanvasMain->Refresh();

	event.Skip();
}

void MainGUI::DeleteAllMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	if (wxMessageBox(wxT("Вы уверены, что хотите удалить все ?"), this->GetTitle(), wxYES_NO | wxCANCEL | wxNO_DEFAULT | wxCENTRE |
		wxICON_INFORMATION) == wxYES)
	{
		FlowchartManager.Clear();
		FlowchartManager.SetModified(true);
		FlowchartCanvasMain->Refresh();
	}

	event.Skip();
}

void MainGUI::SearchMainOnText(wxCommandEvent& event)
{
	if (event.GetString() != "")
	{
		ToolBoxMain_List.Hide();
		ToolBoxMain_List.FindAndShow(event.GetString());
	}
	else
	{
		ToolBoxMain_List.Hide();
		ToolBoxMain_List.Show();
	}

	event.Skip();
}

void MainGUI::ToolBoxMainOnTreeItemActivated(wxTreeEvent& event)
{
	FlowchartCanvasMain->CreateShapeObject(ToolBoxMain->GetItemText(ToolBoxMain->GetSelection()), &FlowchartManager);
	FlowchartCanvasMain->Refresh();

	event.Skip();
}

void MainGUI::OpenMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	wxFileDialog openFileDialog(this, wxT("Открыть файл"), "", "", "VRPF (*.vrpf)|*.vrpf|Все файлы (*.*)|*.*", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openFileDialog.ShowModal() == wxID_CANCEL)
	{
		return;
	}

	try
	{
		FlowchartCanvasMain->LoadCanvas(openFileDialog.GetPath());
	}
	catch (const std::exception&)
	{
		wxMessageBox(wxT("Не удалось открыть файл."), this->GetTitle(), wxOK | wxCENTRE | wxICON_EXCLAMATION);
	}
	m_pathSaveData = openFileDialog.GetPath();

	event.Skip();
}

void MainGUI::SaveMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	SaveData(m_pathSaveData);

	event.Skip();
}

void MainGUI::SaveAsMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	SaveData();

	event.Skip();
}

void MainGUI::NewProjectMenuItemMainOnMenuSelection(wxCommandEvent& event)
{
	if (CheckSaveData() == false)
	{
		return;
	}

	m_pathSaveData = wxEmptyString;
	FlowchartManager.Clear();
	FlowchartCanvasMain->ClearCanvasHistory();
	FlowchartManager.SetModified(false);
	FlowchartCanvasMain->Refresh();

	event.Skip();
}

void MainGUI::MainGUIOnClose(wxCloseEvent& event)
{
	if (CheckSaveData() == false)
	{
		event.Veto();
		return;
	}

	event.Skip();
}

bool MainGUI::CheckSaveData()
{
	if (FlowchartManager.IsModified() == true)
	{
		int tempObject = wxMessageBox(wxT("Вы хотите сохранить изменения ?"), this->GetTitle(), wxYES_NO | wxCANCEL | wxYES_DEFAULT | wxCENTRE |
			wxICON_INFORMATION);
		if (tempObject == wxYES)
		{
			if (SaveData(m_pathSaveData) == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (tempObject == wxNO)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return true;
	}
}

bool MainGUI::SaveData(wxString path)
{
	if (path == wxEmptyString)
	{
		wxFileDialog saveFileDialog(this, wxT("Сохранить файл"), "", "", "VRPF (*.vrpf)|*.vrpf", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
		if (saveFileDialog.ShowModal() == wxID_CANCEL)
			return false;

		try
		{
			FlowchartCanvasMain->SaveCanvas(saveFileDialog.GetPath());
		}
		catch (const std::exception&)
		{
			wxMessageBox(wxT("Не удалось сохранить файл."), this->GetTitle(), wxOK | wxCENTRE | wxICON_EXCLAMATION);
			return false;
		}
		m_pathSaveData = saveFileDialog.GetPath();
	}
	else
	{
		try
		{
			FlowchartCanvasMain->SaveCanvas(m_pathSaveData);
		}
		catch (const std::exception&)
		{
			wxMessageBox(wxT("Не удалось сохранить файл."), this->GetTitle(), wxOK | wxCENTRE | wxICON_EXCLAMATION);
			return false;
		}
	}
	return true;
}

void MainGUI::CancelMenuItemMainOnUpdateUI(wxUpdateUIEvent& event)
{
	bool Undo = FlowchartCanvasMain->CanUndo();
	CancelMenuItemMain->Enable(Undo);

	event.Skip();
}

void MainGUI::BackMenuItemMainOnUpdateUI(wxUpdateUIEvent& event)
{
	bool Redo = FlowchartCanvasMain->CanRedo();
	BackMenuItemMain->Enable(Redo);

	event.Skip();
}

void MainGUI::OnShapeLeftDoubleClick(wxSFShapeMouseEvent& event)
{
	FlowchartCanvasMain->EditShapeObject(event);

	event.Skip();
}

void MainGUI::OnShapeRightClick(wxSFShapeMouseEvent& event)
{
	FlowchartCanvasMain->StartInteractiveConnection(CLASSINFO(wxSFLineShape), event.GetMousePosition());

	event.Skip();
}

void MainGUI::OnCanvasRightClick(wxMouseEvent& event)
{
	if (FlowchartCanvasMain->GetMode() == wxSFShapeCanvas::modeCREATECONNECTION)
	{
		FlowchartCanvasMain->AbortInteractiveConnection();
		return;
	}

	event.Skip();
}

void MainGUI::m_generateCodeOnMenuSelection(wxCommandEvent& event)
{
	wxDirDialog generateDirDialog(this, "Выберите папку", "", wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST | wxDD_CHANGE_DIR);
	if (generateDirDialog.ShowModal() == wxID_CANCEL)
	{
		return;
	}

	std::string path = (std::string)generateDirDialog.GetPath();
	Parser generateCode(&FlowchartManager, path);
	generateCode.Generate();

	event.Skip();
}