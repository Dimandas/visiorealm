﻿#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/srchctrl.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/treectrl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/frame.h>
#include <wx/aui/aui.h>

#include "flowchart/canvas.h"
#include "utils/list.h"


namespace VisioRealm
{
	class MainGUI : public wxFrame
	{

	private:

	protected:
		wxSearchCtrl* SearchMain;
		wxTreeCtrl* ToolBoxMain;
		wxMenuBar* MenubarMain;
		wxMenu* FileMenu1Main;
		wxMenu* EditMenu2Main;
		wxMenu* ViewMenu3Main;
		wxMenu* InformationMenu4Main;

		// Added user code
		wxSFDiagramManager FlowchartManager;
		FlowchartCanvas* FlowchartCanvasMain;
		GeneralListOCV ToolBoxMain_List;
		wxString m_pathSaveData;

		wxMenuItem* CancelMenuItemMain;
		wxMenuItem* BackMenuItemMain;

		bool CheckSaveData();
		bool SaveData(wxString path = wxEmptyString);
		//
		void OnShapeLeftDoubleClick(wxSFShapeMouseEvent& event);
		void OnShapeRightClick(wxSFShapeMouseEvent& event);
		void OnCanvasRightClick(wxMouseEvent& event);
		///////

		// Virtual event handlers, overide them in your derived class
		virtual void MainGUIOnClose(wxCloseEvent& event);
		virtual void SearchMainOnText(wxCommandEvent& event);
		virtual void ToolBoxMainOnTreeItemActivated(wxTreeEvent& event);
		virtual void NewProjectMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void OpenMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void SaveMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void SaveAsMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void m_generateCodeOnMenuSelection(wxCommandEvent& event);
		virtual void ExitMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void CancelMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void CancelMenuItemMainOnUpdateUI(wxUpdateUIEvent& event);
		virtual void BackMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void BackMenuItemMainOnUpdateUI(wxUpdateUIEvent& event);
		virtual void DeleteAllMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void ZoomInMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void ZoomOutMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void DefaultScaleMenuItemMainOnMenuSelection(wxCommandEvent& event);
		virtual void AboutMenuItemMainOnMenuSelection(wxCommandEvent& event) { event.Skip(); }


	public:

		MainGUI(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("VisioRealm"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(950, 650), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);
		wxAuiManager m_mgr;

		~MainGUI();

	};
}