﻿#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <wx/image.h>

namespace VisioRealm
{
	wxImage ConvertBGRAToRGB(cv::Mat& inputBGRA)
	{
		cv::Mat convertedObject;

		cv::cvtColor(inputBGRA, convertedObject, cv::COLOR_BGRA2RGB);

		size_t sizeImage = convertedObject.elemSize()*convertedObject.cols*convertedObject.rows;
		wxImage outputImage(convertedObject.cols, convertedObject.rows, (unsigned char*)malloc(sizeImage));

		unsigned char* transferImageFromOCV = convertedObject.data;
		unsigned char* transferImageToWx = outputImage.GetData();

		for (size_t i = 0; i < sizeImage; i++)
		{
			transferImageToWx[i] = transferImageFromOCV[i];
		}

		return outputImage;
	}
}