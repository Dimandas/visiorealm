﻿#include <algorithm>

#include "list.h"

using namespace VisioRealm;

void GeneralListOCV::Create(wxTreeCtrl* _TreeCtrl, wxTreeItemId _RootItem)
{
	TreeCtrl = _TreeCtrl;
	RootItem = _RootItem;

	ItemActionList.push_back(WrapperTreeItem("Изменить яркость"));

	ItemRequirementList.push_back(WrapperTreeItem("Число A равно числу B"));

	ItemEventList.push_back(WrapperTreeItem("Срабатывание таймера"));

}

void GeneralListOCV::Show()
{
	wxTreeItemId ActionList = TreeCtrl->AppendItem(RootItem, wxT("Действие"));
	for (size_t i = 0; i < ItemActionList.size(); i++)
		ItemActionList[i].Item = TreeCtrl->AppendItem(ActionList, ItemActionList[i].Caption);

	wxTreeItemId RequirementList = TreeCtrl->AppendItem(RootItem, wxT("Условие"));
	for (size_t i = 0; i < ItemRequirementList.size(); i++)
		ItemRequirementList[i].Item = TreeCtrl->AppendItem(RequirementList, ItemRequirementList[i].Caption);

	wxTreeItemId EventList = TreeCtrl->AppendItem(RootItem, wxT("Событие"));
	for (size_t i = 0; i < ItemEventList.size(); i++)
		ItemEventList[i].Item = TreeCtrl->AppendItem(EventList, ItemEventList[i].Caption);

	TreeCtrl->Refresh();
	TreeCtrl->Update();
}

void GeneralListOCV::FindAndShow(wxString FindCaption)
{
	StringRemoveSpaces(FindCaption);
	StringToLowerCase(FindCaption);

	wxString TempComparisonsVariable;

	for (size_t i = 0; i < ItemActionList.size(); i++)
	{
		TempComparisonsVariable = ItemActionList[i].Caption;
		StringRemoveSpaces(TempComparisonsVariable);
		StringToLowerCase(TempComparisonsVariable);

		if (TempComparisonsVariable.find(FindCaption) != wxString::npos)
		{
			ItemActionList[i].Item = TreeCtrl->AppendItem(RootItem, ItemActionList[i].Caption);
			wxApp::GetInstance()->Yield();
		}
	}

	for (size_t i = 0; i < ItemRequirementList.size(); i++)
	{
		TempComparisonsVariable = ItemRequirementList[i].Caption;
		StringRemoveSpaces(TempComparisonsVariable);
		StringToLowerCase(TempComparisonsVariable);
		if (TempComparisonsVariable.find(FindCaption) != wxString::npos)
		{
			ItemRequirementList[i].Item = TreeCtrl->AppendItem(RootItem, ItemRequirementList[i].Caption);
			wxApp::GetInstance()->Yield();
		}
	}

	for (size_t i = 0; i < ItemEventList.size(); i++)
	{
		TempComparisonsVariable = ItemEventList[i].Caption;
		StringRemoveSpaces(TempComparisonsVariable);
		StringToLowerCase(TempComparisonsVariable);
		if (TempComparisonsVariable.find(FindCaption) != wxString::npos)
		{
			ItemEventList[i].Item = TreeCtrl->AppendItem(RootItem, ItemEventList[i].Caption);
			wxApp::GetInstance()->Yield();
		}
	}
	TreeCtrl->Refresh();
	TreeCtrl->Update();
}

void GeneralListOCV::StringRemoveSpaces(wxString& InputString)
{
	InputString.erase(std::remove(InputString.begin(), InputString.end(), ' '),
		InputString.end());
}

void GeneralListOCV::StringToLowerCase(wxString& InputString)
{

}

void GeneralListOCV::Hide()
{
	TreeCtrl->DeleteAllItems();
	TreeCtrl->Refresh();
	TreeCtrl->Update();
}