﻿#pragma once

#include <vector>
#include <wx/treectrl.h>
#include <wx/app.h>


namespace VisioRealm
{
	class GeneralListOCV
	{
	public:
		GeneralListOCV() { ; }
		~GeneralListOCV() { ; }
		void Create(wxTreeCtrl* _TreeCtrl, wxTreeItemId _RootItem);
		void Show();
		void Hide();
		void FindAndShow(wxString FindCaption);
	protected:
		struct WrapperTreeItem
		{
			WrapperTreeItem(const wxString& _Caption) { Caption = _Caption; }
			wxString Caption;
			wxTreeItemId Item;
		};
		std::vector <WrapperTreeItem> ItemActionList;
		std::vector <WrapperTreeItem> ItemRequirementList;
		std::vector <WrapperTreeItem> ItemEventList;
		wxTreeItemId RootItem;
		wxTreeCtrl* TreeCtrl;
		void StringRemoveSpaces(wxString& InputString);
		void StringToLowerCase(wxString& InputString);
	};
}